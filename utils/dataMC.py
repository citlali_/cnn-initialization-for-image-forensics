import numpy as np
import torchvision.datasets as datasets
from torch.utils.data import Dataset, DataLoader
from PIL import Image

class ImgDataset(Dataset):
    def __init__(self, x, y):
        self.x = np.array(x, dtype=np.float32)
        self.y = np.array(y, dtype=np.int64)
    def __getitem__(self, index):
        return self.x[index], self.y[index]
    def __len__(self):
        return len(self.x)

class Data():
    def __init__(self, conf):
        self.conf = conf
        self.data_train_path = conf.data_train_path
        self.data_test_path = conf.data_test_path
        self.train_loader = None
        self.test_loader = None


    
    def openimg(self, path):
        readimg=Image.open(path)
        readimg=np.array(readimg,dtype=np.float32)
        readimg=np.reshape(readimg,(1,64,64))
        readimg/=255.0
        return readimg

    def load_data(self, batch_size=0):
        print("-> load training data from: {}".format(self.data_train_path))
        if not batch_size:
            batch_size = self.conf.batch_size
        print("Batch size",batch_size)

        self.train_loader= DataLoader(datasets.DatasetFolder(root=self.data_train_path,loader=self.openimg,extensions="png"), batch_size=batch_size, shuffle=True, num_workers=6)
        self.test_loader = DataLoader(datasets.DatasetFolder(root=self.data_test_path,loader=self.openimg,extensions="png"), batch_size=batch_size, shuffle=True, num_workers=6)
        return self