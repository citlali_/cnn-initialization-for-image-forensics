import numpy as np
import math
import random
import glob
import torch, torch.nn as nn, torch.nn.functional as F, random
from PIL import Image
import time

class fac_filt:
    def __init__(self, conf):
        self.conf = conf
        self.data_train_path = conf.data_train_path
        self.files= glob.glob(self.data_train_path+'/*/*.png')
        self.covmat, self.var_input=self.get_cov()
        self.patches, self.var_patches=self.get_patches_conv(pct=.01)
        #self.const_weight_test = nn.Parameter(torch.randn(size=[conf.no_initfilters, 1, 5, 5]), requires_grad=False)
        
        
        
    def get_cov(self,ppi=15, pct=.1):
        imno=int(len(self.files)*pct)
        indx=0
        X=np.empty((25,ppi*imno))

        for j in range(imno):
            testimg = np.array(Image.open(self.files[random.randrange(0,len(self.files))]))
            for i in range(ppi):
                randx=random.randrange(0,59)
                randy=random.randrange(0,59)
                X[:,indx]=testimg[randx:randx+5,randy:randy+5].reshape((25,))
                indx+=1
        X=X-128 #Centering the data
        covmat=np.cov(X)
        return covmat, np.var(X)
    
    def get_scaled_lincomb(self,W):
        W=W.reshape(25,)
        #B term
        B=0
        for i in range(25):
            B+=self.covmat[i,i]*W[i]**2
        #Z term
        from itertools import combinations
        indx=[i for i in range(25)]
        comb=combinations(indx,2)
        Z=0
        for pair in comb:
            Z+=W[pair[0]]*W[pair[1]]*self.covmat[pair[0],pair[1]]
        Z*=2
        #print("Z: ",Z)
        var_output=(Z+B)

        factor=math.sqrt(self.var_input/var_output)
        return W.reshape((5,5))*factor
    
    def get_patches_conv(self, pct=.1):
        imno=int(len(self.files)*pct)
        patches=np.empty((imno,64,64))

        for j in range(imno):
            patches[j]= np.array(Image.open(self.files[random.randrange(0,len(self.files))]))

        patches = patches.reshape((imno,1,64,64))
        patches = torch.from_numpy(patches)
        patches = patches.type(torch.float)
        print("a")
        patches = patches.cuda()
        return patches, np.var(patches.cpu().detach().numpy())
    
    def get_scaled_conv(self,filtsrm,pct=.1):
        
        const_weight_test = filtsrm.reshape((1,1,5,5))
        const_weight_test =const_weight_test =const_weight_test.cuda()
        with torch.no_grad():
            conv = F.conv2d(self.patches, const_weight_test)

        var1=self.var_patches
        var2=np.var(conv.cpu().detach().numpy())
        factor=math.sqrt(var1/var2)
        #print("factor conv", factor)

        
        return filtsrm.reshape((5,5))*factor

    
   