import numpy as np
import math
import random
import glob
from PIL import Image
import time

class ihfilter:
    def __init__(self, conf):
        self.conf = conf



    def indep_init(self,sz):
        n=(sz**2)-1

        #Center value
        c=math.sqrt((3*n))
        #var=1/n
        vals=[]
        for i in range(n):
                if c<0:
                    vals.append(random.uniform(0,-c/(n/2)))
                else:
                    vals.append(random.uniform(-c/(n/2), 0))
        weight = np.zeros([sz,sz])

        k=0
        for i in range(sz):
            for j in range(sz):
                if i==math.floor(sz/2) and j==math.floor(sz/2):
                    weight[i,j] = c
                else:
                    weight[i,j] = vals[k]
                    k+=1
        return weight
