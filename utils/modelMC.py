#!/usr/bin/env python
# -*- coding:utf-8 -*-

__author__ = 'ivan'

import torch, torch.nn as nn, torch.nn.functional as F, random
from config.confMC import Conf
import pickle
import numpy as np
from utils.scale_filt import fac_filt
from utils.inits import ihfilter
import copy
import random
import time
np.random.seed(np.uint32(42))
torch.manual_seed(np.uint32(42))
torch.cuda.manual_seed(np.uint32(42))

random.seed(42)

class ModelBase(nn.Module):
    def __init__(self, name, created_time):
        super(ModelBase, self).__init__()
        self.name = name
        self.created_time = created_time

    def copy_params(self, state_dict):
        own_state = self.state_dict()
        for (name, param) in state_dict.items():
            if name in own_state:
                own_state[name].copy_(param.clone())

    def boost_params(self, scale=1.0):
        if scale == 1.0:
            return self.state_dict()
        for (name, param) in self.state_dict().items():
            self.state_dict()[name].copy_((scale * param).clone())
        return self.state_dict()

    # self - x
    def sub_params(self, x):
        own_state = self.state_dict()
        for (name, param) in x.items():
            if name in own_state:
                own_state[name].copy_(own_state[name] - param)

    # self + x
    def add_params(self, x):
        a = self.state_dict()
        for (name, param) in x.items():
            if name in a:
                a[name].copy_(a[name] + param)

class MISLnet(ModelBase):
    def __init__(self, conf=Conf, name=None, created_time=None):
        super(MISLnet, self).__init__('ConstrainedCNN', created_time)


        if conf.Filt_orig == 'indep_init':
            self.const_weight = nn.Parameter(torch.randn(size=[conf.no_initfilters, 1, 5, 5]), requires_grad=True)
            filts=self.const_weight.data
            ihfilt=ihfilter(conf)
            self.scl_filt=fac_filt(conf)
            for i in range(conf.no_initfilters):
                filts[i,0]=torch.from_numpy(ihfilt.indep_init(5))
            self.const_weight.data=filts
        elif conf.Filt_orig == 'depend_init':
            self.scl_filt=fac_filt(conf)
            self.const_weight = nn.Parameter(nn.init.xavier_uniform_(torch.randn(size=[conf.no_initfilters, 1, 5, 5])), requires_grad=True)
            filts=self.const_weight.data
            for i in range(conf.no_initfilters):
                if conf.Filt_scl == "lincomb":
                    filts[i,0]=self.scl_filt.get_scaled_lincomb(filts[i])
                elif conf.Filt_scl == "conv":
                    filts[i,0]=self.scl_filt.get_scaled_conv(filts[i])
            self.const_weight.data=filts








        print("Filters", self.const_weight)

        self.conv1 = nn.Conv2d(conf.no_initfilters, 96, 7, stride=2, padding=3)
        self.bn1=nn.BatchNorm2d(96)#,momentum=0.10,eps=0.001)
        self.conv2 = nn.Conv2d(96, 64, 5, stride=1, padding=2)
        self.bn2=nn.BatchNorm2d(64)#,momentum=0.10,eps=0.001)
        self.conv3 = nn.Conv2d(64, 64, 5, stride=1, padding=2)
        self.bn3=nn.BatchNorm2d(64)#,momentum=0.10,eps=0.001)
        self.conv4 = nn.Conv2d(64, 128, 1, stride=1)
        self.bn4=nn.BatchNorm2d(128)#,momentum=0.10,eps=0.001)
        self.fc1 = nn.Linear(128, 200)
        self.fc2 = nn.Linear(200, 200)
        self.fc3 = nn.Linear(200, conf.total_class)
        self.max_pool = nn.MaxPool2d(kernel_size=3, stride=2,ceil_mode=True)
        self.avg_pool = nn.AvgPool2d(kernel_size=3, stride=2)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_uniform_(m.weight)
                nn.init.constant_(m.bias, 0.1)
            elif isinstance(m, nn.Linear):
                nn.init.xavier_uniform_(m.weight)


    def normalized_S(self,conf=Conf):
        filts=self.const_weight.data
        central_pixel = copy.deepcopy(filts[:, 0, 2, 2])

        for i in range(conf.no_initfilters):
            sumed = filts[i].sum() - central_pixel[i]
            filts[i] /= -sumed/central_pixel[i]
            filts[i,0,2,2]=central_pixel[i]
            if conf.Filt_orig == "Bayar_lincomb":
                filts[i,0]=self.scl_filt.get_scaled_lincomb(filts[i])
            elif conf.Filt_orig == "Bayar_conv":
                filts[i,0]=self.scl_filt.get_scaled_conv(filts[i])
            #print("scaled numpy",filts[i])
        self.const_weight.data=filts

    def normalized_B(self,conf=Conf):
        central_pixel = (self.const_weight.data[:, 0, 2, 2])
        for i in range(conf.no_initfilters):
            sumed = self.const_weight.data[i].sum() - central_pixel[i]
            self.const_weight.data[i] /= sumed
            self.const_weight.data[i, 0, 2, 2] = -1.0


    def forward(self,x,conf=Conf):
        ###UNCOMMNET next line for BAYAR constraint
        if conf.Filt_orig == "Bayar_conv" or conf.Filt_orig == "Bayar_lincomb" :
            with torch.no_grad():
                self.normalized_S()
        elif conf.Filt_orig == 'Bayar':
            with torch.no_grad():
                self.normalized_B()
        x = F.conv2d(x, self.const_weight)
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.max_pool(torch.tanh(x))
        x = self.conv2(x)
        x = self.bn2(x)
        x = self.max_pool(torch.tanh(x))
        x = self.conv3(x)
        x = self.bn3(x)
        x = self.max_pool(torch.tanh(x))
        x = self.conv4(x)
        x = self.bn4(x)
        x = self.avg_pool(torch.tanh(x))

        # Fully Connected
        x = torch.flatten(x,1)
        x = self.fc1(x)
        #print("fc1",x.shape)
        x = torch.tanh(x)
        x = self.fc2(x)
        #print("fc2",x.shape)

        #x = torch.tanh(x)
        logist = self.fc3(torch.tanh(x))
        #print("fc3",logist.shape)

        output = F.softmax(logist,dim=1)
        #output = torch.sigmoid(logist)
        #print("output",output.shape)
        return logist, output


if __name__ == "__main__":
    model = MISLnet(conf=Conf, name="testing")
    model.summary()
