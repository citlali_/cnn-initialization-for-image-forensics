
# coding: utf-8

# In[1]:


import argparse
import numpy as np
import pickle
import datetime
from tqdm import tqdm
import torch
import torch.nn.functional as F
from utils.dataMC import Data
from utils.helper import Helper
from config.confMC import Conf
from utils.modelMC import MISLnet as Model
from torch.utils.tensorboard import SummaryWriter
from sklearn.metrics import accuracy_score
np.random.seed(42)
torch.manual_seed(42)
torch.cuda.manual_seed(42)

import os
os.environ['CUDA_VISIBLE_DEVICES']='1'

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", default="conf")
    return parser.parse_args()




def training(conf, model, loader,criterion, optimizer,trainwriter,epoch):

    model.train()
    y_true = []
    y_pred = []
    for i in loader:
        #LOADING THE DATA IN A BATCH
        data, target = i
        #MOVING THE TENSORS TO THE CONFIGURED DEVICE
        data, target = data.to(conf.device), target.to(conf.device)
        #FORWARD PASS
        logist, _ = model(data.float())
        loss = criterion(logist, target)
        #BACKWARD AND OPTIMIZE
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # PREDICTIONS
        target = np.round(target.detach().cpu())
        y_pred.extend((np.array(logist.detach().cpu())).argmax(axis=1).tolist())
        y_true.extend(target.tolist())
    print("Accuracy on training set is" ,accuracy_score(y_true,y_pred))
    trainwriter.add_scalar('epoch_accuracy',accuracy_score(y_true,y_pred),epoch)




def test(conf, model, test_loader,testwriter,epoch):
    model.eval()

    y_true = []
    y_pred = []

    # set the requires_grad flag to false as we are in the test mode
    with torch.no_grad():
        for i in test_loader:
            #LOAD THE DATA IN A BATCH
            data,target = i

            # moving the tensors to the configured device
            data, target = data.to(conf.device), target.to(conf.device)
            # the model on the data
            logists,output = model(data.float())

            #PREDICTIONS
            pred = np.round(output.cpu())
            target = target.float()
            y_true.extend(target.tolist())
            y_pred.extend((np.array(logists.detach().cpu())).argmax(axis=1).tolist())

    print("Accuracy on test set is" , accuracy_score(y_true,y_pred))
    print("***********************************************************")
    testwriter.add_scalar('epoch_accuracy',accuracy_score(y_true,y_pred),epoch)


# In[4]:


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']


# In[ ]:


def runtraining(filt=0):
    conf = __import__("config." + "confMC", globals(), locals(), ["Conf"]).Conf
    idrun=''
    data = Data(conf)
    data.load_data()
    conf.indxfilt=filt
    model = Model(conf).to(conf.device)
    print(model)

    datestart=datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=conf.learning_rate,weight_decay=0.0005, momentum=0.95)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=6, gamma=0.5)
    if conf.Filt_orig == "File":
        logdir=conf.logdir+datestart+'_'+str(conf.total_class)+'C_NoF='+str(conf.no_initfilters)+'_'+conf.filt_file.split('.')[0]+idrun
    else:
        logdir=conf.logdir+datestart+'_'+str(conf.total_class)+'C_NoF='+str(conf.no_initfilters)+'_'+conf.Filt_orig+idrun
    trainwriter =SummaryWriter(log_dir=logdir+'/train')
    testwriter = SummaryWriter(log_dir=logdir+'/validation')
    fst_lay_filt=np.empty((conf.total_epoch,5,5))

    print("INIT: ")
    print(model.const_weight.data[0, 0, :, :] )
    for epoch in tqdm(range(conf.total_epoch),position=0):
        fst_lay_filt[epoch]=np.array(model.const_weight.data[0, 0, :, :].cpu())
        print("LR: ",get_lr(optimizer))
        #Run training set
        training(conf, model, data.train_loader,criterion, optimizer,trainwriter,epoch)
        #Run testing set
        test(conf,model, data.test_loader, testwriter, epoch)
        #execute scheduler for learning rate with the conditions defined previously
        scheduler.step()
        print("After epoch: ",epoch)
        print(model.const_weight.data[0, 0, :, :])


    print(model.const_weight)
    testwriter.close()
    trainwriter.close()
    pickle.dump(fst_lay_filt,open(logdir+"/filters.obj","wb"))
    torch.save(model.state_dict(), logdir+'/model.pt')


# In[ ]:


for j in range(5):
        runtraining()
