import os, torch, random, math

class Conf(object):
    ROOT = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    total_class = 6
    total_epoch = 60
    batch_size = 64
    learning_rate = 0.001
    no_initfilters=3
    indxfilt=0
    randomfilts=True
    #Options are depend_init or indep_init
    Filt_orig='indep_init'
    #Options are lincombg or conv
    Filt_scl='lincomb'
    logdir = '/path/to/store/logs/F'+str(no_initfilters)
    data_train_path = '/path/to/training/dataset'
    data_test_path = '/path/to/testing/dataset'


    model_path = os.path.join(ROOT, "model")

    data_path = os.path.join(ROOT, "data")

    print("pathroot",ROOT)
    print("pathroot",data_path)
    print("pathroot",model_path)
