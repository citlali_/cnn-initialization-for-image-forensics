
# Code for [Data-dependent scaling of CNN's first layer for improved image manipulation detection](https://hal.archives-ouvertes.fr/hal-03000629/document)  and the data-independent version in pytorch.

Original pytorch code for architecture was taken from [grasses repository](https://github.com/grasses/Constrained-CNN)

Inside the **config/confMC.py** file, several setting should be changed:

- Change **logdir** to the folder path which will store accuracies and the first filter of the first layer.
- Change **data_path** to the path that contains the image database.
- Change **Filt_orig** to use either a data dependent initialization using Xavier method with "depend_init" or independent init with "indep_init" to initialize the first layer
	>In case of a data-dependent initialization, change "Filt_scl" either to "lincomb" for linear combination method or "conv" for convolutional method.
	>In case you want to use another filters and scale them with the data-dependent method, change the contents of  "filts" variable inside utils/modelMC.py
Change "no_initfilters" to the number of desired filters in the first layer.

The accuracy logs after each epoch will be stored in a tensorboard file

Current version uses a data-independent high pass filter initialization.

If you want to obtain a scaled filter independently, **utils/scale_filt.py** file contains the two methods to return a scaled filter. For this, the config/confMC.py should be properly set with the correct data_path.
